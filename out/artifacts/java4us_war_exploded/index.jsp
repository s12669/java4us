<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <title>Demo application</title>
</head>
<body>
<h1>Welcome to the Java 4 US! Conference registration page</h1>
<a href="${pageContext.request.contextPath}/register">Register new participant.</a><br />
<a href="${pageContext.request.contextPath}/participants">Show participants.</a>
</body>
</html>
