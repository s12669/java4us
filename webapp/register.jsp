<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>RegisterForm</title>
</head>
<body>
<h3>Registration form</h3>
<form action="${pageContext.request.contextPath}/register" method="post">
    <div class="form-group">
        <label for="name">First name</label>
        <input type="text" class="form-control" id="name" name="name">
    </div>
    <div class="form-group">
        <label for="surname">Surname</label>
        <input type="text" class="form-control" id="surname" name="surname">
    </div>
    <div class="form-group">
        <label for="email">Email address</label>
        <input type="email" class="form-control" id="email" name="email">
    </div>
    <div class="form-group">
        <label for="employer">Employer</label>
        <input type="text" class="form-control" id="employer" name="employer">
    </div>
    <div class="form-group">
        Where have you heard about us?
    </div>
    <div class="form-group">
        <label for="workAdv">Advertisement at work</label>
        <input type="checkbox" class="form-control" id="workAdv" name="workAdv">
    </div>
    <div class="form-group">
        <label for="schoolAdv">Advertisement at school</label>
        <input type="checkbox" class="form-control" id="schoolAdv" name="schoolAdv">
    </div>
    <div class="form-group">
        <label for="facebook">Facebook</label>
        <input type="checkbox" class="form-control" id="facebook" name="facebook">
    </div>
    <div class="form-group">
        <label for="acquaintance">From acquaintances</label>
        <input type="checkbox" class="form-control" id="acquaintance" name="acquaintance">
    </div>
    <div class="form-group">
        <label for="other">Other</label>
        <input type="checkbox" class="form-control" id="other" name="other">
    </div>
    <div class="form-group">
        <textarea rows='4' cols ='50' name='currJob' /></textarea>
    </div>
    <div class="form-group">
        <label for="occupation">Occupation</label>
        <input type="text" class="form-control" id="occupation" name="occupation">
    </div>
    <button type="submit" class="btn btn-default">Submit</button>
</form>
</body>
</html>
