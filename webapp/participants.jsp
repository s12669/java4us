<%@ page import="com.example.domain.Person" %>
<%@ page import="java.util.ArrayList" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
<%ArrayList<Person> people = (ArrayList<Person>) request.getAttribute("participants");

    for(Person person : people) {
        out.println(person.getFirstName());
        out.println(person.getSurname());
        out.println(person.getEmail());
        out.println(person.getEmployer());
        out.println(person.getOccupation()+"<br />");
    }
%>
</body>
</html>
