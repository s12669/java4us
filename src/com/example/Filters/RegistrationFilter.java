package com.example.filters;

import com.example.service.StorageService;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebFilter(filterName = "RegistrationFilter")
public class RegistrationFilter implements Filter {
    private StorageService storageService;

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        this.storageService = StorageService.getInstance();
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletResponse httpServletResponse = (HttpServletResponse) servletResponse;

        httpServletResponse.setContentType("text/html");

        if (this.storageService.isFull()) {
            httpServletResponse.sendRedirect("/full_conference.jsp");
            return;
        }

        filterChain.doFilter(servletRequest, servletResponse);
    }

    @Override
    public void destroy() {

    }
}