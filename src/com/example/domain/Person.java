package com.example.domain;

public class Person {

    private String firstName;
    private String surname;
    private String email;
    private String employer;
    private String occupation;

    public Person(String firstName, String surname, String email, String employer, String occupation) {
        this.firstName = firstName;
        this.surname = surname;
        this.email = email;
        this.employer = employer;
        this.occupation = occupation;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEmployer() {
        return employer;
    }

    public void setEmployer(String employer) {
        this.employer = employer;
    }

    public String getOccupation() {
        return occupation;
    }

    public void setOccupation(String occupation) {
        this.occupation = occupation;
    }

    public Boolean isValid() {
        return this.firstName != null &&
                this.surname != null &&
                this.email != null &&
                this.employer != null &&
                this.occupation != null;
    }
}

